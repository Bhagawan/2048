package com.example.game2024.game

import kotlin.math.absoluteValue
import kotlin.math.pow
import kotlin.math.sqrt

private const val SPEED = 40.0f
private const val PERFECT_DT = 16.67 * 2

class Animation(private val fromX: Float, private val fromY: Float, private val toX: Float, private val toY: Float, val amount: Int) {
    private var currX = fromX
    private var currY = fromY
    private var moveVectorX = toX - fromX
    private var moveVectorY = toY - fromY
    var running = true

    init {
        val mod = sqrt(moveVectorX.toDouble().pow(2.0) + moveVectorY.toDouble().pow(2.0))
        moveVectorX = ((moveVectorX) / mod).toFloat()
        moveVectorY = ((moveVectorY) / mod).toFloat()
    }

    fun step(dT: Long) {
        val frame = (dT / PERFECT_DT).toFloat()
        if((toX - currX).absoluteValue <= (moveVectorX * SPEED * frame).absoluteValue &&
            (toY - currY).absoluteValue <= (moveVectorY * SPEED * frame).absoluteValue ) {
            currX = toX
            currY = toY
            running = false
        } else {
            currX += moveVectorX * SPEED * frame
            currY += moveVectorY * SPEED * frame
        }
    }

    fun getX(): Float = currX

    fun getY(): Float = currY

}