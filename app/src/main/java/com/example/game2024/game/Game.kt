package com.example.game2024.game

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Point
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import java.util.concurrent.Executors
import kotlin.math.absoluteValue

private const val UP = 0
private const val RIGHT = 1
private const val DOWN = 2
private const val LEFT = 3

class Game(context: Context?, attrs: AttributeSet?) : View(context, attrs) {
    private var gameOn = false
    private var animOn = false
    private var size = 0
    private var squareSize = 0

    private lateinit var field: Array<Array<Int>>
    private lateinit var anims: Array<Array<Animation?>>

    private var touchX = 0.0f
    private var touchY = 0.0f

    private var lastFrame: Long = 0

    private val executor = Executors.newSingleThreadExecutor()
    private val h = Handler(Looper.getMainLooper())

    private var callback: GameInterface? = null

    init {
        executor.execute {
            while(true) {
                h.post {
                    invalidate()
                }
                Thread.sleep(1000 / 60)
            }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        squareSize = if(width < height) width / size else height / size
        super.onSizeChanged(w, h, oldw, oldh)
    }

    override fun onDraw(canvas: Canvas?) {
        if(height > 0 && canvas != null) {

            drawField(canvas)
            val currFrame = System.currentTimeMillis()
            drawSquares(canvas,  currFrame - lastFrame)
            lastFrame = currFrame
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if(gameOn && !animOn){
            when(event?.actionMasked) {
                MotionEvent.ACTION_DOWN -> {
                    touchX = event.x
                    touchY = event.y
                    return true
                }
                MotionEvent.ACTION_UP -> {
                    if((touchX - event.x).absoluteValue > (touchY - event.y).absoluteValue) {
                        if(touchX - event.x >= 0) move(LEFT)
                        else move(RIGHT)
                    } else {
                        if(touchY - event.y >= 0) move(UP)
                        else move(DOWN)
                    }
                    return true
                }
            }
        }
        return super.onTouchEvent(event)
    }

    fun setCallback(callback: GameInterface) {
        this.callback = callback
    }

    fun restart() = startGame(size)

    private fun drawField(c: Canvas) {
        c.drawColor(Color.parseColor("#A6A6A6"))
        val p = Paint()
        p.style = Paint.Style.STROKE
        p.strokeWidth = 2.0f
        p.color = Color.parseColor("#474747")
        for(i in 0 until size) {
            for(j in 0 until size) {
                c.drawRect((squareSize * j).toFloat(),
                    (squareSize * i).toFloat(), (squareSize * (j + 1)).toFloat(), (squareSize * (i + 1)).toFloat(), p)
            }
        }
    }

    private fun drawSquares(c: Canvas, dT: Long) {
        val p = Paint()
        p.color = Color.BLACK
        p.style = Paint.Style.FILL
        p.color = Color.parseColor("#C5A980")
        p.textAlign = Paint.Align.CENTER
        for(i in 0 until size) {
            for(j in 0 until size) {
                if(field[i][j] != 0 || anims[i][j] != null) {
                    var left = (squareSize * j).toFloat()
                    var top = (squareSize * i).toFloat()
                    var amount = field[i][j]
                    val a = anims[i][j]
                    if(a != null) {
                        a.step(dT)
                        left = a.getX()
                        top = a.getY()
                        amount = a.amount
                        if(!a.running) {
                            getSquareAt(left, top).let { point ->
                                if(field[point.x][point.y] == 0) field[point.x][point.y] = a.amount
                                else field[point.x][point.y] *= 2
                            }
                            anims[i][j] = null
                            if(checkAnims()) spawnNumber()
                        }
                    }
                    p.textSize = squareSize  * 0.75f / (amount.toString().length + 1)
                    p.color = Color.rgb(if((197 + amount) < 255) 197 + amount else 255, 169, 128)
                    c.drawRect(left + 4, top + 4, left + squareSize -4, top + squareSize - 4, p)
                    p.color = Color.BLACK
                    c.drawText(amount.toString(), left + squareSize * 0.5f, top + (squareSize + p.textSize) / 2, p)
                }
            }
        }
    }

    private fun spawnNumber() {
        val empty = ArrayList<Int>()

        for(i in 0 until (size - 1)) {
            if(field[0][i] == 0) empty.add(i)
            if(field[size - 1 - i][0] == 0) empty.add((size - 1) * 3 + i)
            if(field[i][size - 1] == 0) empty.add((size - 1) + i)
            if(field[size - 1][size - 1 - i] == 0) empty.add((size - 1) * 2 + i)
        }
        empty.sort()

        if(empty.isNotEmpty()) {
            val r = empty.random()
            val i = r / (size - 1)
            val j = r % (size - 1)
            when(i) {
                0 -> field[0][j] = 2
                1 -> field[j][size - 1] = 2
                2 -> field[size - 1][size - 1 - j] = 2
                3 -> field[size - 1 - j][0] = 2
            }
        }
        if(empty.size <= 1) {
            if(checkEnd()) {
                invalidate()
                endGame()
            }
        }
        callback?.currentScore(getScore())
    }

    private fun move(direction: Int) {
        val tempF : Array<Array<Int>> = Array(field.size) { Array(field.size) { 0 } }
        for(i in tempF.indices) {
            for(j in tempF[i].indices) {
                tempF[i][j] = field[i][j]
            }
        }
        var dist: Int
        when(direction) {
            UP -> {
                for(i in 1 until size) {
                    for (j in 0 until size) {
                        if(tempF[i][j] != 0) {
                            dist = 0
                            for(n in (i - 1) downTo 0) {
                                when(tempF[n][j]) {
                                    tempF[i][j] -> {
                                        dist++
                                        break
                                    }
                                    0 -> dist++
                                    else -> break
                                }
                            }
                            if(dist > 0) {
                                if(tempF[i - dist][j] == 0) tempF[i - dist][j] = tempF[i][j] else tempF[i - dist][j] *= 2
                                anims[i][j] = Animation((squareSize * j).toFloat(),
                                    (squareSize * i).toFloat(), (squareSize * j).toFloat(), (squareSize * (i - dist)).toFloat(), field[i][j])
                                tempF[i][j] = 0
                                field[i][j] = 0
                                animOn = true
                            }
                        }

                    }
                }
            }
            RIGHT -> {
                for(j in size - 2 downTo 0) {
                    for (i in 0 until size) {
                        if(tempF[i][j] != 0) {
                            dist = 0
                            for (n in (j + 1) until size) {
                                when (tempF[i][n]) {
                                    tempF[i][j] -> {
                                        dist++
                                        break
                                    }
                                    0 -> dist++
                                    else -> break
                                }
                            }
                            if (dist > 0) {
                                if(tempF[i][j + dist] == 0) tempF[i][j + dist] = tempF[i][j] else tempF[i][j + dist] *= 2
                                animOn = true
                                anims[i][j] = Animation(
                                    (squareSize * j).toFloat(),
                                    (squareSize * i).toFloat(),
                                    (squareSize * (j + dist)).toFloat(),
                                    (squareSize * i).toFloat(),
                                    field[i][j]
                                )
                                tempF[i][j] = 0
                                field[i][j] = 0
                            }
                        }
                    }
                }
            }
            DOWN -> {
                for(i in size - 2 downTo 0) {
                    for (j in 0 until size) {
                        if(tempF[i][j] != 0) {
                            dist = 0
                            for (n in (i + 1) until size) {
                                when (tempF[n][j]) {
                                    tempF[i][j] -> {
                                        dist++
                                        break
                                    }
                                    0 -> dist++
                                    else -> break
                                }
                            }
                            if (dist > 0) {
                                if(tempF[i + dist][j] == 0) tempF[i + dist][j] = tempF[i][j] else tempF[i + dist][j] *= 2
                                animOn = true
                                anims[i][j] = Animation(
                                    (squareSize * j).toFloat(),
                                    (squareSize * i).toFloat(),
                                    (squareSize * j).toFloat(),
                                    (squareSize * (i + dist)).toFloat(),
                                    field[i][j]
                                )
                                tempF[i][j] = 0
                                field[i][j] = 0
                            }
                        }
                    }
                }
            }
            LEFT -> {
                for(j in 1 until size) {
                    for (i in 0 until size) {
                        if(tempF[i][j] != 0) {
                            dist = 0
                            for (n in (j - 1) downTo 0) {
                                when (tempF[i][n]) {
                                    tempF[i][j] -> {
                                        dist++
                                        break
                                    }
                                    0 -> dist++
                                    else -> break
                                }
                            }
                            if (dist > 0) {
                                if(tempF[i][j - dist] == 0) tempF[i][j - dist] = tempF[i][j] else tempF[i][j - dist] *= 2
                                animOn = true
                                anims[i][j] = Animation(
                                    (squareSize * j).toFloat(),
                                    (squareSize * i).toFloat(),
                                    (squareSize * (j - dist)).toFloat(),
                                    (squareSize * i).toFloat(),
                                    field[i][j]
                                )
                                tempF[i][j] = 0
                                field[i][j] = 0
                            }
                        }
                    }
                }
            }
        }
    }

    private fun checkEnd() : Boolean {
        val tempF : Array<Array<Int>> = Array(field.size) { Array(field.size) { 0 } }
        for(i in tempF.indices) {
            for(j in tempF[i].indices) {
                tempF[i][j] = field[i][j]
            }
        }
        var dist: Int
        for(i in 1 until size) {
            for (j in 0 until size) {
                if(tempF[i][j] != 0) {
                    dist = 0
                    for(n in (i - 1) downTo 0) {
                        when(tempF[n][j]) {
                            tempF[i][j] -> {
                                dist++
                                break
                            }
                            0 -> dist++
                            else -> break
                        }
                    }
                    if(dist > 0) return false
                }

            }
        }
        for(j in size - 2 downTo 0) {
            for (i in 0 until size) {
                if(tempF[i][j] != 0) {
                    dist = 0
                    for (n in (j + 1) until size) {
                        when (tempF[i][n]) {
                            tempF[i][j] -> {
                                dist++
                                break
                            }
                            0 -> dist++
                            else -> break
                        }
                    }
                    if (dist > 0) return false
                }
            }
        }
        for(i in size - 2 downTo 0) {
            for (j in 0 until size) {
                if(tempF[i][j] != 0) {
                    dist = 0
                    for (n in (i + 1) until size) {
                        when (tempF[n][j]) {
                            tempF[i][j] -> {
                                dist++
                                break
                            }
                            0 -> dist++
                            else -> break
                        }
                    }
                    if (dist > 0) return false
                }
            }
        }
        for(j in 1 until size) {
            for (i in 0 until size) {
                if(tempF[i][j] != 0) {
                    dist = 0
                    for (n in (j - 1) downTo 0) {
                        when (tempF[i][n]) {
                            tempF[i][j] -> {
                                dist++
                                break
                            }
                            0 -> dist++
                            else -> break
                        }
                    }
                    if (dist > 0) return false
                }
            }
        }
        return true
    }

    fun startGame(size: Int) {
        this.size = size
        field = Array(size) { Array(size) { 0 } }
        anims = Array(size) { Array(size) { null } }
        gameOn = true
        spawnNumber()
        spawnNumber()
        executor.execute {
            while(gameOn) {
                h.post {
                    invalidate()
                }
                Thread.sleep(1000 / 60)
            }
        }
    }

    private fun checkAnims() : Boolean{
        for(j in 0 until size) {
            for (i in 0 until size) if (anims[i][j] != null) return false
        }
        animOn = false
        return true
    }

    private fun getScore(): Int {
        var score = 0
        for(i in 0 until size) {
            for (j in 0 until size) score += field[i][j]
        }
        return score
    }

    private fun endGame() {
        gameOn = false
        callback?.onEnd(getScore())
    }

    private fun getSquareAt(x: Float, y: Float) : Point = Point((y / squareSize).toInt(), (x / squareSize).toInt())

    interface GameInterface {
        fun onEnd(score: Int)
        fun currentScore(score: Int)
    }
}