package com.example.game2024

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import android.widget.LinearLayout
import android.widget.PopupWindow
import androidx.appcompat.widget.AppCompatButton
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.game2024.databinding.ActivityMainBinding
import com.example.game2024.util.RecordsAdapter
import com.example.game2024.util.SharedPref
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var target: Target

    override fun onBackPressed() {
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        fullscreen()
        setBackground()

        binding.btnNewGame.setOnClickListener { showNewGamePopupWindow() }
        binding.btnRecords.setOnClickListener { showRecordsPopupWindow() }
    }

    private fun showNewGamePopupWindow() {
        val inflater = layoutInflater
        val popupView = inflater.inflate(R.layout.popup_size_choise, null)

        val width = LinearLayout.LayoutParams.MATCH_PARENT
        val height = LinearLayout.LayoutParams.MATCH_PARENT

        val popupWindow = PopupWindow(popupView, width, height, false)

        popupWindow.animationStyle = R.style.PopupAnimation
        popupWindow.showAtLocation(binding.root, Gravity.CENTER, 0, 0)
        popupView.findViewById<AppCompatButton>(R.id.btn_popup_4).setOnClickListener {
            val intent = Intent(this, GameActivity::class.java)
            intent.putExtra("size", 4)
            startActivity(intent)
            popupWindow.dismiss()
        }
        popupView.findViewById<AppCompatButton>(R.id.btn_popup_6).setOnClickListener {
            val intent = Intent(this, GameActivity::class.java)
            intent.putExtra("size", 6)
            startActivity(intent)
            popupWindow.dismiss()
        }
        popupView.findViewById<AppCompatButton>(R.id.btn_popup_10).setOnClickListener {
            val intent = Intent(this, GameActivity::class.java)
            intent.putExtra("size", 10)
            startActivity(intent)
            popupWindow.dismiss()
        }
        popupView.setOnClickListener { popupWindow.dismiss() }

        val p = popupWindow.contentView.rootView.layoutParams as WindowManager.LayoutParams
        p.flags = p.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        p.dimAmount = 0.26f

        val wm: WindowManager = this.getSystemService(Context.WINDOW_SERVICE) as WindowManager

        wm.updateViewLayout(popupWindow.contentView.rootView, p)
    }

    private fun showRecordsPopupWindow() {
        val inflater = layoutInflater
        val popupView = inflater.inflate(R.layout.records_popup, null)

        val width = LinearLayout.LayoutParams.MATCH_PARENT
        val height = binding.root.height

        val popupWindow = PopupWindow(popupView, width, height, false)

        popupWindow.animationStyle = R.style.PopupAnimation
        popupWindow.showAtLocation(binding.root, Gravity.CENTER, 0, 0)

        val recycler : RecyclerView = popupView.findViewById(R.id.recycler_records)
        recycler.layoutManager = LinearLayoutManager(this)
        recycler.adapter = RecordsAdapter(SharedPref.getRecords(this))

        popupView.setOnClickListener { popupWindow.dismiss() }

        val p = popupWindow.contentView.rootView.layoutParams as WindowManager.LayoutParams
        p.flags = p.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        p.dimAmount = 0.26f

        val wm: WindowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager

        wm.updateViewLayout(popupWindow.contentView.rootView, p)
    }

    private fun fullscreen() {
        if (Build.VERSION.SDK_INT >= 30) {
            binding.root.windowInsetsController?.hide(WindowInsets.Type.statusBars() or WindowInsets.Type.navigationBars())
            binding.root.windowInsetsController?.systemBarsBehavior = WindowInsetsController.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        } else {
            binding.root.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LOW_PROFILE or
                        View.SYSTEM_UI_FLAG_FULLSCREEN or
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                        View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                        View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        }
    }

    private fun setBackground() {
        target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                binding.layoutMain.background = BitmapDrawable(resources, bitmap)
            }
            override fun onBitmapFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {}
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
        }
        Picasso.get().load("http://195.201.125.8/2048/back.png").into(target)
    }
}