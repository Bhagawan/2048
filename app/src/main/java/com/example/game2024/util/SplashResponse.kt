package com.example.game2024.util

import androidx.annotation.Keep

@Keep
data class SplashResponse(val url : String)

