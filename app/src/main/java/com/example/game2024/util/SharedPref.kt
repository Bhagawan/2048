package com.example.game2024.util

import android.content.Context
import android.content.SharedPreferences
import java.util.*
import kotlin.collections.HashSet

class SharedPref {
    companion object {

        fun addRecord(context: Context, score: Int) {
            val sP: SharedPreferences =
                context.getSharedPreferences("Records", Context.MODE_PRIVATE)
            val currentRecords = sP.getStringSet("records", HashSet())
            var newRecordsSet = currentRecords?.let { HashSet(it) }
            if(newRecordsSet?.size!! > 100) {
                newRecordsSet = HashSet(newRecordsSet.sorted().dropLast(newRecordsSet.size - 100))
            }
            newRecordsSet.add(score.toString())
            sP.edit().putStringSet("records", newRecordsSet).apply()
        }

        fun getRecords(context : Context) : List<String> {
            val sp = context.getSharedPreferences("Records", Context.MODE_PRIVATE)
            return sp.getStringSet("records", HashSet<String>())?.sortedByDescending { it.toInt() } ?: ArrayList()
        }
    }
}