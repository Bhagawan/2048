package com.example.game2024

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageButton
import com.example.game2024.databinding.ActivityGameBinding
import com.example.game2024.game.Game
import com.example.game2024.util.SharedPref
import com.squareup.picasso.Target

class GameActivity : AppCompatActivity() {
    private lateinit var binding: ActivityGameBinding
    private lateinit var target: Target

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGameBinding.inflate(layoutInflater)
        setContentView(binding.root)
        fullscreen()

        binding.game.setCallback(object: Game.GameInterface {
            override fun onEnd(score: Int) {
                SharedPref.addRecord(this@GameActivity, score)
                showEndNotification(score)
            }

            override fun currentScore(score: Int) {
                binding.textGameCurrentScore.text = score.toString()
            }
        })
        binding.game.startGame(intent.getIntExtra("size", 4))
        val list = SharedPref.getRecords(this)

        binding.textGameBestScore.text = if(list.isEmpty()) "0" else list.first()

        binding.imageButtonExit.setOnClickListener { finish() }
        binding.imageButtonRetry.setOnClickListener { binding.game.restart() }
    }

    @SuppressLint("InflateParams")
    private fun showEndNotification(score: Int) {
        val inflater = layoutInflater
        val popupView = inflater.inflate(R.layout.popup_end_game, null)

        val width = LinearLayout.LayoutParams.MATCH_PARENT
        val height = LinearLayout.LayoutParams.MATCH_PARENT

        val popupWindow = PopupWindow(popupView, width, height, false)

        popupWindow.animationStyle = R.style.PopupAnimation
        popupWindow.showAtLocation(binding.root, Gravity.CENTER, 0, 0)
        popupView.findViewById<TextView>(R.id.text_end_score).text = score.toString()
        popupView.findViewById<AppCompatImageButton>(R.id.btn_end_exit).setOnClickListener {
            popupWindow.dismiss()
            finish()
        }
        popupView.findViewById<AppCompatImageButton>(R.id.btn_end_replay).setOnClickListener {
            binding.game.restart()
            popupWindow.dismiss()
        }

        val p = popupWindow.contentView.rootView.layoutParams as WindowManager.LayoutParams
        p.flags = p.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        p.dimAmount = 0.26f

        val wm: WindowManager = this.getSystemService(Context.WINDOW_SERVICE) as WindowManager

        wm.updateViewLayout(popupWindow.contentView.rootView, p)
    }

    private fun fullscreen() {
        if (Build.VERSION.SDK_INT >= 30) {
            binding.root.windowInsetsController?.hide(WindowInsets.Type.statusBars() or WindowInsets.Type.navigationBars())
            binding.root.windowInsetsController?.systemBarsBehavior = WindowInsetsController.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        } else {
            binding.root.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LOW_PROFILE or
                        View.SYSTEM_UI_FLAG_FULLSCREEN or
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                        View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                        View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        }
    }
}